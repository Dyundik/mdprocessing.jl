#Вывести частицы расстояние между которыми меньше 6 ,а P2 > 0.9 для всех таких пар построить распределение P2
using LinearAlgebra

using MDProcessing

using Plots

function p2(n1, n2)
    cos_angle = dot(n1, n2)

    return 1.5 * cos_angle^2 - 0.5
end

function mol_res(system::MDState; max_radius::Real, P2_threshold::Real)
    output = [(0, 0) for _ in 1:0]
    nrm = system[:normal]

    for p in neighbor_pairs(system, max_radius)
        i, j = atom_indices(p)

        p_res = p2(nrm[i], nrm[j])

        if p_res > P2_threshold
            push!(output, (i, j))
        end
    end
    return output
end


function p2_distribution(system::MDState; max_radius::Real)
    dist = Float64[]
    nrm = system[:normal]

    for p in neighbor_pairs(system, max_radius)
        i, j = atom_indices(p)

        p_res = p2(nrm[i], nrm[j])
        push!(dist, p_res)

    end
    return dist
end

function main()
    n_hist_bins = 60
    dp2 = 1.5 / n_hist_bins
    amount = zeros(n_hist_bins, 3)
    tstep_max = parse(Int, ARGS[2])
    output = traj_average(ARGS[1]; timesteps=0:tstep_max) do system

        mols = molecule_planes(system)

        #output = mol_res(mols; max_radius = 6.0, P2_threshold = 0.9)
        
        data = p2_distribution(mols; max_radius = 6.0)

        for p2_val in data
            bin_index = floor(Int, (p2_val + 0.5) / dp2)
            amount[begin+bin_index, 2] += 1
        end

        @views amount[:, 2] ./= sum(amount[:, 2]) * dp2

        amount[:, 1] = (-0.5 + dp2 / 2) : dp2 : 1.0

        @views amount[:, 3] .= amount[:, 2] .* sqrt.(6 .* amount[:, 1] .+ 3)
        return amount
    end

    foreach(println, eachrow(output))

    @info "" sum(output[:, 2]) * dp2
 
    #b_range  = range(-0.5, 1, length = 100)

    #histogram(data, label ="P2", bins=b_range, normalize=:pdf, color=:gray)

    #title!("P2 distribution")
    #xlabel!("P2 value")
    #ylabel!("amount")

    #savefig("hist.png")

    out_name = split(ARGS[1], '*')[1] * ".data"

    buf = IOBuffer()

    foreach(eachrow(output)) do r
        println(buf, round(r[1]; digits=4), '\t', r[2], '\t', r[3])
    end
    
    open(out_name, "w") do io
        print(io, String(take!(buf)))
    end
end

main()
