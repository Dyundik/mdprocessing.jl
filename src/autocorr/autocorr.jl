module Autocorr

using LinearAlgebra, FFTW

export acf_fft, integrate

"""
    acf_fft(v::AbstractVector{<:Real}[; upto=length(v)])

Compute autocorrelation defined as `ACF[t] = ⟨v[i]v[i+t-1]⟩` using the fast Fourier
    transform method.
"""
function acf_fft(arr::AbstractVector{T}; upto::Integer = length(arr)) where {T<:Real}
    n = length(arr)
    upto = min(n, upto)
    F = float(T)
    acf = append!(Vector{F}(arr), Iterators.repeated(0, n))
    afft = FFTW.r2r!(acf, FFTW.R2HC)
    i2n = inv(2 * n)
    @inbounds afft[1] *= afft[1] * i2n
    @inbounds for i in 0:n-2
        afft[2+i] = (afft[2+i]^2 + afft[end-i]^2) * i2n
    end
    @inbounds afft[n+1] *= afft[n+1] * i2n
    afft[n+2:end] .= 0
    acf = FFTW.r2r!(acf, FFTW.HC2R)
    resize!(acf, n)
    for i in eachindex(acf)
        acf[i] /= (n - i + 1)
    end
    return acf[1:upto]
end

"""
    acf_fft(vecs[; upto=length(v)])

Compute `[acf_fft(v; upto) for v in vecs]` using saved FFT plans for efficiency.
"""
acf_fft(arrs; upto = nothing) = __acf_fft__(arrs, upto)

function __acf_fft__(arrs, ::Nothing)
    F = float(eltype(eltype(arrs)))
    acfs = Vector{F}[]
    acf = F[]
    nplan = -1
    local FFT_dir
    local FFT_inv
    for arr in arrs
        n = length(arr)
        if 2n != nplan
            resize!(acf, 2n)
            nplan = 2n
            FFT_dir = FFTW.plan_r2r!(acf, FFTW.R2HC)
            FFT_inv = FFTW.plan_r2r!(acf, FFTW.HC2R)
        end
        acf[1:n] .= arr
        acf[n+1:end] .= 0
        afft = FFT_dir * acf
        @inbounds afft[1] *= afft[1] / (2 * n)
        @inbounds for i in 0:n-2
            afft[2+i] = (afft[2+i]^2 + afft[end-i]^2) / (2 * n)
        end
	@inbounds afft[n+1] *= afft[n+1] / (2 * n)
        afft[n+2:end] .= 0
        acf = FFT_inv * afft
        @inbounds for i in 1:n
            acf[i] /= n - i + 1
        end
        push!(acfs, acf[1:n])
    end
    return acfs
end

"""
    integrate(acf[; dt=1.0])

Compute the running integral of `acf` using the trapezoid rule.
"""
function integrate(acf::AbstractVector{T}; dt=one(float(T))) where T<:Real
    F = promote_type(float(T), typeof(dt))
    running_integ = zeros(F, length(acf))
    c = zero(F)
    for idx in 2:lastindex(acf)
        # running Kahan summation
        x = (acf[idx-1] + acf[idx]) / 2
        s = running_integ[idx-1]
        y = x - c
        tmp = s + y
        c = (tmp - s) - y
        running_integ[idx] = tmp
    end
    running_integ .*= dt
    return running_integ
end

end # module
