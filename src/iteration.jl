Base.IteratorEltype(::Type{<:MDState}) = Base.HasEltype()

Base.eltype(T::Type{<:MDState}) = MDParticle{T}

Base.IteratorSize(T::Type{<:MDState}) = Base.HasLength()

Base.@propagate_inbounds function Base.iterate(state::MDState, idx::Int=1)
    idx in eachindex(state.coord) || return nothing
    return MDParticle(state, idx), idx+1
end

Base.length(state::MDState) = length(state.coord)

"""
    filter(predicate, system::MDState)

Return a copy of `system` which contains only the particles satisfying the `predicate`.
"""
function Base.filter(fn, system::MDState{T}) where {T}
    keep = [fn(part) for part in system]
    filtered = MDState{T}(boxsize=system.size)
    append!(filtered.id, system.id[keep])
    append!(filtered.type, system.type[keep])
    append!(filtered.mol, system.mol[keep])
    append!(filtered.coord, system.coord[keep])
    append!(filtered.vel, system.vel[keep])
    for (k, v) in system._scalars
       addproperty!(filtered; name=k, data=v[keep], allow_sharing=true)
    end
    for (k, v) in system._vectors
        addproperty!(filtered; name=k, data=v[keep], allow_sharing=true)
    end
    return filtered
end
