mag2(vec) = sum(x->x^2, vec)

"""
    dot3(a, b, c)

Return the scalar-vector product `dot(a, cross(b, c))` of length-3 vectors.
"""
Base.@propagate_inbounds function dot3(a, b, c)
    @boundscheck if !(length(a) == length(b) == length(c) == 3)
        throw(DimensionMismatch("length-3 vectors expected"))
    end

    @inbounds prod = a[1] * (b[2] * c[3] - b[3] * c[2]) +
        a[2] * (b[3] * c[1] - b[1] * c[3]) +
        a[3] * (b[1] * c[2] - b[2] * c[1])
    return prod
end

kin_energy(velocities) = sum(mag2, velocities) / 2

function kin_energy(system::MDState)
    if hasproperty(system, :mass)
        m = system.mass
        v = system.vel
        return sum(i -> m[i] * v[i]^2, eachindex(m, v)) / 2
    else
        kin_energy(system.vel)
    end
end

"""
    dist(r1, r2, size)

Compute the distance between `r1` and `r2` in a box with size `size` assuming periodic
    boundary conditions.
"""
function dist(r1::AbstractVector, r2::AbstractVector, size)
    Δr = r1 .- r2
    if length(size) != length(Δr)
        throw(DimensionMismatch("expected length-$(length(size)) vectors, got $(Δr)"))
    end
    Δr .%= size
    for i in eachindex(Δr)
        if Δr[i] > size[i] / 2
            Δr[i] -= size[i]
        elseif Δr[i] < -size[i] / 2
            Δr[i] += size[i]
        end
    end
    return norm(Δr)
end

function dist(
    r1::StaticVector{N,T}, r2::StaticVector{N,T}, size::StaticVector{N}
) where {N,T<:Real}
    Δr = (r1 - r2) .% size
    halfsize = size / 2
    Δr = ifelse.(Δr .> halfsize, Δr - size, Δr)
    Δr = ifelse.(Δr .< -halfsize, Δr + size, Δr)
    return norm(Δr)
end

function dist2(
    r1::StaticVector{N,T}, r2::StaticVector{N,T}, size::StaticVector{N}
) where {N,T<:Real}
    Δr = (r1 - r2) .% size
    halfsize = size / 2
    Δr = ifelse.(Δr .> halfsize, Δr - size, Δr)
    Δr = ifelse.(Δr .< -halfsize, Δr + size, Δr)
    return mag2(Δr)
end

function coss(r1, r2)
    q = mag2(r1)
    w = mag2(r2)
    return (r1 ⋅ r2) / sqrt(q * w)
end

"""
    integrate_trap!(runsum::AbstractVector, input::AbstractVector, dx::Number)

Fill `runsum` with the running integral values of `input`. The integration is performed by
    the trapezoid method, and `input` should contain the values of function at points
    separated by `dx`.
"""
function integrate_trap!(runsum::AbstractVector, input::AbstractVector, dx::Number)
    for n in 2:length(runsum)
        runsum[n] = runsum[n-1] + 0.5 * dx * (input[n-1] + input[n])
    end
    return runsum
end

"""
    integrate_trap(input::AbstractVector, dx::Number)

Return an array of running integral values of `input`. The integration is performed by
    the trapezoid method, and `input` should contain the values of function at points
    separated by `dx`.
"""
function integrate_trap(input::AbstractVector{T}, dx::Number=one(T)) where {T<:Number}
    runsum = similar(input, float(T))
    return integrate_trap!(runsum, input, dx)
end
