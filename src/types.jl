const VecVec{T} = Vector{SVector{3,T}}

mutable struct MDState{T <: AbstractFloat}
    id::Vector{Int}
    type::Vector{Int}
    mol::Vector{Int}
    coord::VecVec{T}
    vel::VecVec{T}
    box_vectors::SMatrix{3,3,T,9}
    origin::SVector{3,T}
    pbc::NTuple{3,Bool}
    _scalars::Dict{Symbol, Vector{T}}
    _vectors::Dict{Symbol, VecVec{T}}
    cell_list::Array{Vector{Int}, 3}
end

"""
    MDState{T<:AbstractFloat}

The data structure to store a state of MD simulation.

    MDState{[T=Float64]}(
        ;
        boxdim=(0, 0, 0),
        cell_vectors=[
            1 0 0
            0 1 0
            0 0 1
        ],
        origin=(0, 0, 0),
        pbc=(true, true, true),
        cutoff=0,
    )

Create a new MDState.
# Keywords
- `boxdim`: number of cell vectors in each dimension of the simulation box (3-tuple or
    length-3 vector, default: `(0, 0, 0)`)
- `cell_vectors`: cell vectors as columns in a form of a matrix
- `pbc`: periodic boundaries along three box vectors (`true` / `false`)
- `cutoff`: cut-off radius to build the initial cell list (default: 0)
"""
function MDState{T}(
    ;
    box_vectors=SMatrix{3,3,T}(I),
    origin=(0, 0, 0),
    cutoff::Real=0,
    pbc=(true, true, true),
) where {T<:AbstractFloat}
    boxvol = abs(det(box_vectors))
    abox, bbox, cbox = eachcol(box_vectors)
    area_yz = norm(cross(bbox, cbox))
    area_xz = norm(cross(abox, cbox))
    area_xy = norm(cross(abox, bbox))

    lx, ly, lz = boxvol ./ (area_yz, area_xz, area_xy)
    if cutoff <= 0
        ncells = (0, 0, 0)
    elseif cutoff > min(lx, ly, lz) / 2
        @warn "Cut-off radius is too large, reducing to the half of the minimum box dimension"
        ncells = (4, 4, 4)
    else
        ncells = floor.(Int, 2 .* (lx, ly, lz) ./ cutoff)
    end
    return MDState{T}(
        Int[],
        Int[],
        Int[],
        VecVec{T}(),
        VecVec{T}(),
        box_vectors,
        origin,
        pbc,
        Dict{Symbol, Vector{T}}(),
        Dict{Symbol, VecVec{T}}(),
        [Int[] for _ in CartesianIndices(ncells)]
    )
end

MDState(args...) = MDState{Float64}(args...)

MDState(; kwargs...) = MDState{Float64}(; kwargs...)

const STATE_FIELDS = fieldnames(MDState)

@inline function Base.getproperty(buf::MDState{T}, p::Symbol) where {T}
    if p in STATE_FIELDS
        return getfield(buf, p)
    else
        scal_attr = get(getfield(buf, :_scalars), p, nothing)
        scal_attr !== nothing && return scal_attr

        vec_attr = get(getfield(buf, :_vectors), p, nothing)
        vec_attr === nothing && error("Property $p not found in the MD state")
        return vec_attr
    end
end

function Base.propertynames(buf::MDState; private::Bool = false)
    base_properties = setdiff(STATE_FIELDS, (:_scalars, :_vectors, :cell_list))
    if private
        append!(base_properties, (:_scalars, :_vectors, :cell_list))
    end
    return append!(base_properties, keys(buf._scalars), keys(buf._vectors))
end

function Base.show(io::IO, state::MDState)
    outfmt = Format("""
    MDState with %d atoms""")
    format(
        io,
        outfmt,
        length(state.coord),
    )
    isempty(state._scalars) || print(
        io,
        ", extra scalar properties: ", keys(state._scalars),
    )
    isempty(state._vectors) || print(
        io,
        ", extra vector properties: ", keys(state._scalars),
    )
end

"""
    addproperty!(state::MDState; name, data::AbstractVecOrMat, allow_sharing=false)

Add a new property with the given `name` to `state` and initialize it with `data`. The new
    property name should not already be present in `state`.

If `data` is a vector, then the new scalar or vector property is added depending on
    `eltype(data)`. If `data` is a 3xN matrix, then a vector property is added.

If the flag `allow_sharing` is set to `true`, `state` stores a reference to `data` when
    possible. If the flag is set to `false` (default), an independent copy of `data` is
    added.
"""
function addproperty!(
    state::MDState;
    name, data::AbstractVecOrMat, allow_sharing::Bool=false,
)
    return addproperty!(state, name, data, allow_sharing)
end

function addproperty!(
    state::MDState,
    name::Union{Symbol, AbstractString, AbstractChar},
    data::AbstractArray,
    allow_sharing::Bool=false
)
    key = Symbol(name)
    if key in STATE_FIELDS || haskey(state._scalars, key) || haskey(state._vectors, key)
        throw(ArgumentError("Property $name already exists"))
    end
    return __addproperty_validkey!(state, key, data, allow_sharing)
end


function __addproperty_validkey!(
    state::MDState{T},
    key::Symbol,
    data::AbstractVector{<:Number},
    allow_sharing::Bool,
) where {T}
    if length(data) != length(state.coord)
        throw(DimensionMismatch(
            "expected $(length(state.coord))-element input, got length $(length(data))"
        ))
    end

    state._scalars[key] = allow_sharing ? data : T.(data)
    return state
end

function __addproperty_validkey!(
    state::MDState{T},
    key::Symbol,
    data::AbstractMatrix{Ta},
    allow_sharing::Bool,
) where {T, Ta<:Number}
    if size(data) != (3, length(state.coord))
        nr, nc = size(data)
        throw(DimensionMismatch(
            "expected 3x$(length(state.coord)) matrix, got $(nr)x$(nc)"
        ))
    end

    state._vectors[key] = SVector{3,T}.(reinterpret(reshape, NTuple{3,Ta}, data))
    return state
end

function __addproperty_validkey!(
    state::MDState{T},
    key::Symbol,
    data::AbstractVector{<:Union{AbstractVector, NTuple{3,Number}}},
    allow_sharing::Bool,
) where {T}
    if length(data) != length(state.coord)
        throw(DimensionMismatch(
            "expected $(length(state.coord))-element input, got length $(length(data))"
        ))
    end

    state._vectors[key] = allow_sharing ? data : SVector{3,T}.(data)
    return state
end

@inline Base.getindex(buf::MDState, p::Symbol) = getproperty(buf, p)

"""
    MDParticle

A type for iteration over MDState.
"""
struct MDParticle{S<:MDState}
    state::S
    idx::Int
end

@inline function Base.getproperty(part::MDParticle, p::Symbol)
    state = getfield(part, :state)
    idx = getfield(part, :idx)
    if p in (:id, :type, :mol, :coord, :vel)
        return getfield(state, p)[idx]
    else
        scal_attr = get(getfield(state, :_scalars), p, nothing)
        scal_attr !== nothing && return scal_attr[idx]

        vec_attr = get(getfield(state, :_vectors), p, nothing)
        vec_attr === nothing && error("Property $p not found for the particle")
        return vec_attr[idx]
    end
end

@inline Base.getindex(part::MDParticle, p::Symbol) = getproperty(part, p)

function Base.propertynames(part::MDParticle; private::Bool = false)
    state = getfield(part, :state)
    base_properties = setdiff(STATE_FIELDS, (:_scalars, :_vectors, :cell_list))
    if private
        append!(base_properties, (:state, :idx))
    end
    return append!(base_properties, keys(state._scalars), keys(state._vectors))
end

function Base.show(io::IO, part::MDParticle)
    state = getfield(part, :state)
    outfmt = Format("""
    MDParticle: ID=%d, type=%d, mol=%d, r=[%.4g, %.4g, %.4g], v=[%.4g, %.4g, %.4g]""")
    format(
        io,
        outfmt,
        part.id,
        part.type,
        part.mol,
        part.coord...,
        part.vel...,
    )
    isempty(state._scalars) || print(
        io,
        ", extra scalar properties: ", keys(state._scalars),
    )
    isempty(state._vectors) || print(
        io,
        ", extra vector properties: ", keys(state._scalars),
    )
end
